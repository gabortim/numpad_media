# Numpad media controls ⌨🔊

Use numpad media buttons for changing the volume levels of the system.

## Usage

Disable the NumLock on the physical keyboard to enable virtual media button emulation.


## Default keymap

- Numpad 7 => Volume down 🔉
- Numpad 8 => Media play/pause ⏯
- Numpad 9 => Volume up 🔊
- Numpad 4 => Play previous media ⏮
- Numpad 6 => Play next media ⏭