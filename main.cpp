#include <windows.h>
#include <iostream>
#ifdef ALTERNATIVE_METHOD
#include <mmdeviceapi.h>
#include <endpointvolume.h>
#endif

using namespace std;

#define VOLUME_CHANGE_STEP 0.02f


void print_usage();
void print_status();
LRESULT CALLBACK LowLevelKeyboardProc(int, WPARAM, LPARAM);
bool handle_key_event(DWORD, bool, bool);
void send_key_event(WORD, bool, bool);
#ifdef ALTERNATIVE_METHOD
IAudioEndpointVolume* init_AudioEndpoint();
void increase_volume();
void decrease_volume();
#endif

HHOOK hook_handle = nullptr;


int main() {
    print_usage();

    //needed for the first print
    print_status();

    hook_handle = SetWindowsHookEx(WH_KEYBOARD_LL, &LowLevelKeyboardProc, GetModuleHandle(nullptr), 0);

    if (hook_handle == nullptr) {
        cout<< "Error" <<endl;
        return 1;
    }

    MSG messages;

    while (GetMessage(&messages, nullptr, 0, 0)) {
        TranslateMessage(&messages);
        DispatchMessage(&messages);
    }

    UnhookWindowsHookEx(hook_handle);
    cout<< "Program end" <<endl;
    return 0;
}

void print_usage() {
    cout<< "Key map:" <<endl;
    cout<< "\tNumpad 7 => Volume down" <<endl;
    cout<< "\tNumpad 8 => Media play/pause" <<endl;
    cout<< "\tNumpad 9 => Volume up" <<endl;
    cout<< "\tNumpad 4 => Play previous media" <<endl;
    cout<< "\tNumpad 6 => Play next media" <<endl<<endl;
}

void print_status() {
    if (GetKeyState(VK_NUMLOCK) & 1) {
        cout << "\r\33[2K\u001b[31mDisable NumLock to activate virtual keys!\u001b[0m";
    } else {
        cout << "\r\33[2KWaiting for keypresses...";
    }
}

LRESULT CALLBACK LowLevelKeyboardProc(int hook_proc_code, WPARAM event_type, LPARAM event_ptr_param) {
    if (hook_proc_code == HC_ACTION) {
        auto event_ptr = reinterpret_cast<KBDLLHOOKSTRUCT *>(event_ptr_param);

        DWORD keycode = event_ptr->vkCode;
        bool extended = event_ptr->flags & LLKHF_EXTENDED;
        bool released = event_ptr->flags & LLKHF_UP;

        print_status();

        bool handled = handle_key_event(keycode, extended, released);

        if (handled)
            return 1;
    }

    return CallNextHookEx(hook_handle, hook_proc_code, event_type, event_ptr_param);
}

bool handle_key_event(DWORD keycode, bool extended, bool released) {
    // NUM_7 => VOL_DOWN
    if (keycode == VK_HOME && !extended && !released) {
#ifndef ALTERNATIVE_METHOD
        send_key_event(VK_VOLUME_DOWN, false, released);
#else
        decrease_volume();
#endif

        return true;
    }

    // NUM_9 => MEDIA_PAUSE
    if (keycode == VK_UP && !extended) {
        send_key_event(VK_MEDIA_PLAY_PAUSE, false, released);
        return true;
    }

    // NUM_8 => VOL_UP
    if (keycode == VK_PRIOR && !extended && !released) {
#ifndef ALTERNATIVE_METHOD
        send_key_event(VK_VOLUME_UP, false, released);
#else
        increase_volume();
#endif
        return true;
    }

    // NUM_4 => MEDIA_PREV
    if (keycode == VK_LEFT && !extended) {
        send_key_event(VK_MEDIA_PREV_TRACK, false, released);
        return true;
    }

    // NUM_6 => MEDIA_NEXT
    if (keycode == VK_RIGHT && !extended) {
        send_key_event(VK_MEDIA_NEXT_TRACK, false, released);
        return true;
    }

    return false;
}

void send_key_event(WORD keycode, bool extended, bool released) {
    INPUT input;
    ZeroMemory(&input, sizeof(input));

    input.type = INPUT_KEYBOARD;
    input.ki.wVk = keycode;
    input.ki.dwFlags = 0;

    if (extended) input.ki.dwFlags |= KEYEVENTF_EXTENDEDKEY;
    if (released) input.ki.dwFlags |= KEYEVENTF_KEYUP;

    UINT sent_count = SendInput(1, &input, sizeof(INPUT));

    if (sent_count != 1)
        cerr << "Failed to send keypress: " << GetLastError() << endl;
}

#ifdef ALTERNATIVE_METHOD
/**
 * Source: https://docs.microsoft.com/hu-hu/archive/blogs/larryosterman/how-do-i-change-the-master-volume-in-windows-vista
 */
IAudioEndpointVolume *init_AudioEndpoint() {
    CoInitialize(NULL);
    IMMDeviceEnumerator *deviceEnumerator = NULL;

    CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER, __uuidof(IMMDeviceEnumerator),
                     (LPVOID *) &deviceEnumerator);

    IMMDevice *defaultDevice = NULL;
    deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);
    deviceEnumerator->Release();
    deviceEnumerator = NULL;
    IAudioEndpointVolume *endpointVolume = NULL;

    defaultDevice->Activate(__uuidof(IAudioEndpointVolume), CLSCTX_INPROC_SERVER, NULL,
                            (LPVOID *) &endpointVolume);

    defaultDevice->Release();
    defaultDevice = NULL;

    return endpointVolume;
}

void increase_volume() {
    IAudioEndpointVolume *endpointVolume = init_AudioEndpoint();

    float currentVolume = 0;
    endpointVolume->GetMasterVolumeLevelScalar(&currentVolume);
    endpointVolume->SetMasterVolumeLevelScalar(min(1.0f, currentVolume + VOLUME_CHANGE_STEP), NULL);

    WINBOOL mute;
    endpointVolume->GetMute(&mute);
    if (mute) {
        endpointVolume->SetMute(false, NULL);
    }

    endpointVolume->Release();
    CoUninitialize();
}

void decrease_volume() {
    IAudioEndpointVolume *endpointVolume = init_AudioEndpoint();

    float currentVolume = 0;
    endpointVolume->GetMasterVolumeLevelScalar(&currentVolume);

    const float newVolume = max(0.0f, currentVolume - VOLUME_CHANGE_STEP);
    endpointVolume->SetMasterVolumeLevelScalar(newVolume, NULL);

    if (newVolume == 0) {
        endpointVolume->SetMute(true, NULL);
    }

    endpointVolume->Release();
    CoUninitialize();
}
#endif